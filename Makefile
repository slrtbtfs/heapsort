CC = gcc
CFLAGS = -O2 -g
OUT = bin/heapsort

heapsort:
	$(CC) $(CFLAGS) -o $(OUT) heapsort.c
